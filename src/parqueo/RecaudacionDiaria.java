/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author CompuStore
 */
public class RecaudacionDiaria implements IImprimir{
    List<Vehiculo>ListaDeVehiculos;
    public RecaudacionDiaria(){
        this.ListaDeVehiculos=new ArrayList<>();
    }
    public void IngresarVehiculo(Vehiculo vehiculo){
        this.ListaDeVehiculos.add(vehiculo);
    }

    @Override
    public void Imprimir() {
         double subtotal = 0;
        for (Vehiculo Vehiculo:ListaDeVehiculos){
            subtotal = subtotal + Vehiculo.getCantidadDeHoras()*Vehiculo.getPrecioParqueo();
            System.out.println(Vehiculo.getPlaca()+Vehiculo.getTipo());
        }
        System.out.println(subtotal);
    
    }
}
