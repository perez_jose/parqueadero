/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueo;

/**
 *
 * @author CompuStore
 */
public class Vehiculo {
    private String Tipo;
    private String Placa;
    private double PrecioParqueo;
    private double CantidadDeHoras;

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public double getPrecioParqueo() {
        return PrecioParqueo;
    }

    public void setPrecioParqueo(double PrecioParqueo) {
        this.PrecioParqueo = PrecioParqueo;
    }

    public double getCantidadDeHoras() {
        return CantidadDeHoras;
    }

    public void setCantidadDeHoras(double CantidadDeHoras) {
        this.CantidadDeHoras = CantidadDeHoras;
    }

   
   
    
}
